#!/bin/sh
set -e

cp /tmp/index.php /var/www/html

rsync -rlgoDv  --delete --exclude 'config.ini.php' --exclude '.htaccess' /tmp/matomo /var/www/html/

#Start Apache in the foreground
echo "Starting httpd";

exec httpd -D FOREGROUND
