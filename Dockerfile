FROM alpine:3.8
LABEL authors=""

ENV MATOMO_CORE_VERSION=piwik-2.16.5
#ENV MATOMO_CORE_VERSION=matomo-3.0.0
#ENV MATOMO_CORE_VERSION=matomo-3.13.6


RUN apk add --update --no-cache bash \
	curl \
	rsync \
	curl-dev \
	php5-intl \
	php5-openssl \
	php5-dba \
	php5-sqlite3 \
	php5-pear \
	php5-phpdbg \
	php5-gmp \
	php5-pdo_mysql \
	php5-pcntl \
	php5-common \
	php5-xsl \
	php5-fpm \	
	php5-mysql \
	php5-mysqli \
	php5-enchant \
	php5-pspell \
	php5-snmp \
	php5-doc \
	php5-dev \
	php5-xmlrpc \
	php5-embed \
	php5-xmlreader \
	php5-pdo_sqlite \
	php5-exif \
	php5-opcache \
	php5-ldap \
	php5-posix \	
	php5-gd  \
	php5-gettext \
	php5-json \
	php5-xml \
	php5 \
	php5-iconv \
	php5-sysvshm \
	php5-curl \
	php5-shmop \
	php5-odbc \
	php5-phar \
	php5-pdo_pgsql \
	php5-imap \
	php5-pdo_dblib \
	php5-pgsql \
	php5-pdo_odbc \
	#php5-xdebug \
	php5-zip \
	php5-apache2 \
	php5-cgi \
	php5-ctype \
	php5-mcrypt \
	php5-wddx \
	php5-bcmath \
	php5-calendar \
	php5-dom \
	php5-sockets \
	php5-soap \
	php5-apcu \
	php5-sysvmsg \
	php5-zlib \
	php5-ftp \
	php5-sysvsem \ 
	php5-pdo \
	php5-bz2 \
	php5-mysqli \
	apache2 \
	libxml2-dev \
	apache2-utils


RUN ln -s /usr/bin/php5 /usr/bin/php

RUN  rm -rf /var/cache/apk/*  && \
    chmod -R 777 /var/www && \
    mkdir /var/www/html && \
    chmod -R 777 /var/www/html && \
    chmod -R 777 /var/lib  && \
    chmod -R 777 /var/log  && \
    chmod -R 777 /var/tmp  && \
    chown -R apache:apache /var/www/html 


# Download and extract Matomo, skip certificate verification
RUN curl -k -L -o /tmp/matomo.zip \
    https://builds.matomo.org/piwik-2.16.5.zip \
    && cd /tmp \
    && unzip matomo.zip \
    && rm matomo.zip

#RUN wget --no-check-certificate https://builds.matomo.org/matomo-${MATOMO_CORE_VERSION}.zip -O /tmp/matomo-${MATOMO_CORE_VERSION}.zip && \
#    cd /tmp && \
#    unzip matomo-${MATOMO_CORE_VERSION}.zip && \
#    rm matomo-${MATOMO_CORE_VERSION}.zip

# Create a directory for Matomo and set it as the working directory
WORKDIR /var/www/html

# Create a symbolic link to make Matomo accessible via /var/www/html/piwik
#RUN chown -R apache:apache  /var/www/html/matomo && \
#    chmod -R 777 /var/www/html/matomo && \
#    ln -s /var/www/html/matomo /var/www/html/piwik


# AllowOverride ALL
RUN sed -i '264s#AllowOverride None#AllowOverride All#' /etc/apache2/httpd.conf
#Rewrite Moduble Enable
RUN sed -i 's#\#LoadModule rewrite_module modules/mod_rewrite.so#LoadModule rewrite_module modules/mod_rewrite.so#' /etc/apache2/httpd.conf
# Document Root to /var/www/html/
RUN sed -i 's#/var/www/localhost/htdocs#/var/www/html#g' /etc/apache2/httpd.conf
# Configure Apache
RUN echo "ServerName localhost" >> /etc/apache2/httpd.conf && \
    sed -i 's/^Listen 80$/Listen 8080/' /etc/apache2/httpd.conf

#needed for piwik
RUN sed -i 's#memory_limit = 128M#memory_limit = 256M#' /etc/php5/php.ini
RUN sed -i '/^;\s*always_populate_raw_post_data = -1/s/^;//' /etc/php5/php.ini

COPY httpd.conf /etc/apache2/conf.d/10-httpd.conf
COPY index.html /var/www/html/index.html
COPY index.php /tmp/index.php
COPY docker-entrypoint.sh /docker-entrypoint.sh

#Start apache
RUN mkdir -p /run/apache2  && \
    chmod -R 777 /run/apache2

WORKDIR  /var/www/html/

EXPOSE 8080

CMD ["sh", "/docker-entrypoint.sh"]

#CMD /usr/sbin/httpd  -D   FOREGROUND
