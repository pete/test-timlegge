#!/bin/sh
set -e

ls -asl /etc/httpd/conf.d/

ps -efl
ls -asl /usr/sbin
#Start Apache in the foreground
echo "Starting httpd";

exec httpd -D FOREGROUND
